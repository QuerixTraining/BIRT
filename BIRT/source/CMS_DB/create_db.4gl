##########################################################################
# BIRT Project						                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#DATABASE auth --please connect to the database, where new DB (birt_demo) will be created, and specify its name in this string 
GLOBALS
	DEFINE status_str STRING
	DEFINE message_out STRING
	DEFINE work_db_name, connect_db_name VARCHAR(32)
END GLOBALS	
MAIN
 define test_db VARCHAR(32)
 
LET work_db_name = "birt_demo"
LET connect_db_name = "auth"   #Database will be used only for connect to DB server. To be able to drop or create BIRT demo database: birt_demo  

OPEN WINDOW DB_Creation WITH FORM "w_form" ATTRIBUTE(BORDER)
MENU
	ON ACTION "Recreate Database"
		CALL drop_database()		
		CALL create_database()
		CALL create_db_tables()
		CALL populate_db_tables()

	ON ACTION "Create Database"
		CALL create_database()

	ON ACTION "Create Tables"
		CALL create_db_tables()

	ON ACTION "Populate tables"
		CALL populate_db_tables()
				
	ON ACTION "Drop DataBase"
		CALL drop_database()
		
	ON ACTION "Drop Tables"
		CALL drop_db_tables()
		
	ON ACTION "Exit"
		EXIT MENU
END MENU
END MAIN

FUNCTION drop_database()
	DATABASE connect_db_name
	CALL display_out("Dropping database.")
	WHENEVER ERROR CONTINUE
		DROP DATABASE birt_demo
			IF STATUS < 0 THEN
				LET status_str = "STATUS = ", STATUS
				CALL display_out(status_str)
				CALL display_out("Database was not found or locked") 
			ELSE 
				CALL display_out("Database was dropped")
			END IF
			CALL empty_line()
	WHENEVER ERROR STOP
	CLOSE DATABASE
END FUNCTION

FUNCTION create_database()
	DATABASE connect_db_name
	CALL display_out("Creating database.")
	WHENEVER ERROR CONTINUE
	CREATE DATABASE birt_demo
			IF STATUS < 0 THEN
				LET status_str = "STATUS = ", STATUS 
				CALL display_out(status_str)
				CALL display_out("Cannot create database.")
				CALL display_out("Possibly you tried to create a database with the same name as one that already exists.")
			ELSE 
				CALL display_out("Database was created")
			END IF
			CALL empty_line()
	WHENEVER ERROR STOP
	CLOSE DATABASE
END FUNCTION

FUNCTION check_table_status(table_name,operation)
DEFINE table_name,operation VARCHAR(32)
	IF STATUS < 0 THEN
		CALL display_out("STATUS = "||STATUS)
		CALL display_out("Cannot "||UPSHIFT(operation)||" table "||table_name)
	ELSE
		CALL display_out(UPSHIFT(operation)||" "||table_name||" operation was successful.")
		IF operation = "load" THEN 
			CALL display_out(SQLCA.SQLERRD[3]||" lines was loaded")
		END IF 
	END IF
END FUNCTION

FUNCTION drop_db_tables()
	DATABASE work_db_name
	WHENEVER ERROR CONTINUE

	DROP TABLE contact
	CALL check_table_status("contact","drop")
	DROP TABLE company
	CALL check_table_status("company","drop")
	DROP TABLE invoice
	CALL check_table_status("invoice","drop")
	DROP TABLE currency
	CALL check_table_status("curency","drop")
	DROP TABLE invoice_line
	CALL check_table_status("invoice_line","drop")
	DROP TABLE stock_item
	CALL check_table_status("stock_item","drop")  
	DROP TABLE pay_methods
	CALL check_table_status("pay_methods","drop")  
	DROP TABLE account
	CALL check_table_status("account","drop")
	
	CALL display_out("All tables were dropped")
	CALL empty_line()  
	
	WHENEVER ERROR STOP
	CLOSE DATABASE
END FUNCTION

FUNCTION create_db_tables()
	WHENEVER ERROR CONTINUE
	DATABASE work_db_name

  CREATE TABLE contact (
    cont_id SERIAL NOT NULL,
    cont_title VARCHAR(10),
    cont_name VARCHAR(20) NOT NULL,
    cont_fname VARCHAR(20),
    cont_lname VARCHAR(20),
    cont_addr1 VARCHAR(40),
    cont_addr2 VARCHAR(40),
    cont_addr3 VARCHAR(40),
    cont_city VARCHAR(20),
    cont_zone VARCHAR(15),
    cont_zip VARCHAR(15),
    cont_country VARCHAR(40),
    cont_phone VARCHAR(15),
    cont_fax VARCHAR(15),
    cont_mobile VARCHAR(15),
    cont_email VARCHAR(50) NOT NULL,
    cont_dept VARCHAR(15),
    cont_org INTEGER,
    cont_position VARCHAR(15),
    cont_picture BYTE,
    cont_password VARCHAR(15),
    cont_ipaddr VARCHAR(15),
    cont_usemail SMALLINT,
    cont_usephone SMALLINT,
    cont_notes CHAR(1000),
    PRIMARY KEY (cont_id)
  )
	CALL check_table_status("contact","create")
  CREATE TABLE company (
    comp_id SERIAL NOT NULL,
    comp_name CHAR(100) NOT NULL,
    comp_addr1 CHAR(40),
    comp_addr2 CHAR(40),
    comp_addr3 CHAR(40),
    comp_city CHAR(20),
    comp_zone CHAR(15),
    comp_zip CHAR(15),
    comp_country CHAR(40),
    acct_mgr INTEGER,
    comp_link INTEGER,
    comp_industry INTEGER,
    comp_priority INTEGER,
    comp_type INTEGER,
    comp_main_cont INTEGER NOT NULL,
    comp_url VARCHAR(50),
    comp_notes CHAR(1000),
    PRIMARY KEY (comp_id)
  )
	CALL check_table_status("company","create")
  CREATE TABLE invoice (
    invoice_id SERIAL NOT NULL,
    account_id INTEGER NOT NULL,
    invoice_date DATE NOT NULL,
    pay_date DATE,
    status INTEGER NOT NULL,
    tax_total MONEY(8,2) NOT NULL,
    net_total MONEY(8,2) NOT NULL,
    inv_total MONEY(8,2) NOT NULL,
    currency_id INTEGER,
    for_total MONEY(8,2),
    operator_id INTEGER NOT NULL,
    pay_method_id INTEGER NOT NULL,
    pay_method_name CHAR(20),
    pay_method_desc CHAR(20),
    del_address_dif INTEGER NOT NULL,
    del_address1 CHAR(30),
    del_address2 CHAR(30),
    del_address3 CHAR(30),
    del_method INTEGER,
    invoice_po CHAR(20)
  )
	CALL check_table_status("invoice","create")
  CREATE TABLE currency (
    currency_id SERIAL NOT NULL,
    currency_name CHAR(10) NOT NULL,
    xchg_rate DECIMAL(5,3) NOT NULL
  )
	CALL check_table_status("curency","create")
  CREATE TABLE invoice_line (
    invoice_id INTEGER NOT NULL,
    quantity INTEGER,
    stock_id CHAR(10) NOT NULL,
    item_tax INTEGER NOT NULL
  )
	CALL check_table_status("invoice_line","create")
  CREATE TABLE stock_item (
    stock_id CHAR(10) NOT NULL,
    item_desc CHAR(80) NOT NULL,
    item_cost MONEY(8,2),
    rate_id INTEGER NOT NULL,
    quantity INTEGER
  )
	CALL check_table_status("stock_item","create")
  CREATE TABLE account (
    account_id SERIAL NOT NULL,
    comp_id INTEGER NOT NULL,
    credit_limit MONEY(8,2),
    discount DECIMAL(4,2) NOT NULL
  )
	CALL check_table_status("account","create")
  CREATE TABLE pay_methods (
    pay_method_id SERIAL NOT NULL,
    pay_method_name CHAR(10) NOT NULL,
    pay_method_desc CHAR(20) NOT NULL
  )
	CALL check_table_status("pay_methods","create")

	CALL display_out("All tables were created")
	CALL empty_line()  

	CLOSE DATABASE
	WHENEVER ERROR STOP
END FUNCTION

FUNCTION load_data(table_name)
DEFINE table_name VARCHAR(32)
DEFINE ins_str VARCHAR(32)
WHENEVER ERROR CONTINUE
IF fgl_test("e", table_name||".unl") THEN
	LET ins_str = "INSERT INTO ", table_name
	LOAD FROM table_name||".unl" ins_str
END IF
WHENEVER ERROR STOP
END FUNCTION

FUNCTION populate_db_tables()

	DATABASE work_db_name  

	CALL load_data("contact")
	CALL check_table_status("contact","load")
	CALL load_data("company")
	CALL check_table_status("company","load")
	CALL load_data("invoice")
	CALL check_table_status("invoice","load")
	CALL load_data("currency")
	CALL check_table_status("currency","load")
	CALL load_data("invoice_line")
	CALL check_table_status("invoice_line","load")
	CALL load_data("stock_item")
	CALL check_table_status("stock_item","load")
	CALL load_data("account")
	CALL check_table_status("account","load")
	CALL load_data("pay_methods")
	CALL check_table_status("pay_methods","load")
	CALL empty_line()
	CLOSE DATABASE
END FUNCTION

FUNCTION display_out(message_str)
	DEFINE message_str STRING
	
	LET message_out = message_out, "\n", message_str
	DISPLAY message_out TO f1
	 
END FUNCTION 

FUNCTION empty_line()
	LET message_out = message_out, "\n"
END FUNCTION
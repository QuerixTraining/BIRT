##########################################################################
# BIRT Project						                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

DATABASE birt_demo
import java java.util.logging.Level
import java org.eclipse.birt.core.framework.Platform
import java org.eclipse.birt.report.engine.api.EngineConfig
import java org.eclipse.birt.report.engine.api.HTMLRenderOption

import java org.eclipse.birt.report.engine.api.HTMLCompleteImageHandler
import java org.eclipse.birt.report.engine.api.HTMLServerImageHandler

import java org.eclipse.birt.report.engine.api.IReportEngine
import java org.eclipse.birt.report.engine.api.IReportEngineFactory
import java org.eclipse.birt.report.engine.api.IReportRunnable
import java org.eclipse.birt.report.engine.api.IRunAndRenderTask
import java org.eclipse.birt.report.engine.api.RenderOption

main
    define config     EngineConfig
	define factory    IReportEngineFactory
	define engine     IReportEngine
	define design     IReportRunnable  
	define task       IRunAndRenderTask
	define html_opts  HTMLRenderOption --HTML
	define birt_home_dir string
	define image_dir string
    define image_url string
    define infile string
    define html_output string
    define server_image_handler HTMLServerImageHandler
    define complete_image_handler HTMLCompleteImageHandler
    define html_rtl boolean
    define html_emb boolean
	DEFINE tmp_param CHAR(20)


	OPEN WINDOW w WITH FORM "birt_param" ATTRIBUTE(BORDER)
	DECLARE cur CURSOR FOR SELECT informix.invoice_line.invoice_id
								FROM informix.invoice_line ORDER BY 1
	FOREACH cur INTO tmp_param
		CALL fgl_list_set("tmp_param",tmp_param,tmp_param)
	END FOREACH
	INITIALIZE tmp_param TO NULL

	LET tmp_param = 1001
	INPUT BY NAME tmp_param ATTRIBUTE(WITHOUT DEFAULTS)

	let birt_home_dir = fgl_getenv("LYCIA_DIR") || "\ReportEngine"
	let config = EngineConfig.create()
	call config.setBIRTHome(birt_home_dir)	
	call Platform.startup(config)
	        
    LET factory = Platform.createFactoryObject("org.eclipse.birt.report.engine.ReportEngineFactory")
    let engine = factory.createReportEngine(config)  
    let infile="17410/invoice.rptdesign"  
    let image_dir="public/image"
    let image_url="image"
    let html_output="public/invoice.html"
	
	-- open BIRT report form
	let design = engine.openReportDesign(infile) 
	let task = engine.createRunAndRenderTask(design)
	 
	    CALL task.setParameterValue("P2",tmp_param)	         
    call task.validateParameters() 

	
	let server_image_handler = HTMLServerImageHandler.create()
    let complete_image_handler = HTMLCompleteImageHandler.create()
    
     --creates HTML as an output file
	   let html_opts = HTMLRenderOption.create()
	
	   call html_opts.setOutputFileName(html_output)
	   call html_opts.setOutputFormat("html")
	
	   let html_rtl = false
       call html_opts.setHtmlRtLFlag(html_rtl)
       let html_emb = false
       call html_opts.setEmbeddable(html_emb)    
       call html_opts.setImageDirectory(image_dir)    
       call html_opts.setBaseImageURL(image_url)
       call html_opts.setImageHandler(server_image_handler)      
	   call task.setRenderOption(html_opts)	
       display "HTML render task is running..."
	   call task.run() 
       display "Report has been saved into folder:"
       display "C:\\ProgramData\\Querix\Lycia\\progs\\17410"
       
       CALL displayIntoBrowser("{CONTEXT}/public/invoice.html")
       call task.close()
       call engine.destroy()           
       call Platform.shutdown()
end main 
       
         -- Function to display the report via Browser 
FUNCTION  displayIntoBrowser(inFileName)

    DEFINE inFileName STRING,
           br1 VARCHAR(300)
        DEFER INTERRUPT
        CLOSE WINDOW w
        OPEN WINDOW rep_win WITH FORM '17410/browser_form' ATTRIBUTE(BORDER)
        let br1 = inFileName
        DISPLAY BY NAME  br1
        CALL fgl_getkey()
        CLOSE WINDOW rep_win
END FUNCTION 

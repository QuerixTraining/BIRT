IMPORT JAVA java.util.logging.Level
IMPORT JAVA org.eclipse.birt.core.framework.Platform
IMPORT JAVA org.eclipse.birt.report.engine.api.EngineConfig
IMPORT JAVA org.eclipse.birt.report.engine.api.HTMLRenderOption
IMPORT JAVA org.eclipse.birt.report.engine.api.IReportEngine
IMPORT JAVA org.eclipse.birt.report.engine.api.IReportEngineFactory
IMPORT JAVA org.eclipse.birt.report.engine.api.IReportRunnable
IMPORT JAVA org.eclipse.birt.report.engine.api.IRunAndRenderTask
IMPORT JAVA org.eclipse.birt.report.model.api.ReportDesignHandle
IMPORT JAVA org.eclipse.birt.report.model.api.ElementFactory
IMPORT JAVA org.eclipse.birt.report.model.api.OdaDataSourceHandle
IMPORT JAVA org.eclipse.birt.report.model.api.SlotHandle
MAIN
	MENU
		ON ACTION "Report from rh-informix2.qx"
			CALL Create_birt("rh2")
		ON ACTION "Report from rh-informix3.qx"
			CALL Create_birt("rh3")
		ON ACTION "EXIT"
			EXIT MENU
	END MENU
END MAIN
FUNCTION Create_birt(DB)
	DEFINE config     EngineConfig
	DEFINE factory    IReportEngineFactory
	DEFINE engine     IReportEngine
	DEFINE design     IReportRunnable  
	DEFINE task       IRunAndRenderTask
	DEFINE html_opts  HTMLRenderOption --HTML
	DEFINE birt_home_dir,filename STRING
	DEFINE report,designHandle ReportDesignHandle 
	DEFINE designFactory ElementFactory 
	DEFINE dsHandle OdaDataSourceHandle 
	DEFINE slot SlotHandle
	DEFINE DB CHAR(3)
     	   
    LET birt_home_dir = fgl_getenv("LYCIA_DIR") || "\ReportEngine"
	LET config = EngineConfig.create()
	CALL config.setBIRTHome(birt_home_dir)	
	CALL Platform.startup(config)
    LET factory = Platform.createFactoryObject("org.eclipse.birt.report.engine.ReportEngineFactory")
	LET engine = factory.createReportEngine(config)       
	LET design = engine.openReportDesign("Birt_no_param.rptdesign")
    
    LET report = design.getDesignHandle()
    LET designFactory=report.getElementFactory( )
    #Set Datasource
	LET dsHandle = designFactory.newOdaDataSource( "Data Source", "org.eclipse.birt.report.data.oda.jdbc" )
	#Set driver
	CALL    dsHandle.setProperty( "odaDriverClass", "com.informix.jdbc.IfxDriver" )
	#Select Target database for report
	CASE DB
		WHEN "rh2"
    		CALL dsHandle.setProperty( "odaURL", "jdbc:informix-sqli://rh-informix2.qx:10012/stores: informixserver=querix_tcp")
			CALL dsHandle.setProperty( "odaUser", "informix")
			CALL dsHandle.setProperty( "odaPassword", "default2375")
			LET filename = "Birt_no_param2qx.html"
		WHEN "rh3"
			CALL    dsHandle.setProperty( "odaURL", "jdbc:informix-sqli://rh-informix3.qx:10013/stores: informixserver=querix_test")
			CALL dsHandle.setProperty( "odaUser", "informix")
			CALL dsHandle.setProperty( "odaPassword", "default2375")
			LET filename = "Birt_no_param3qx.html"
	END CASE
    LET slot = report.getDataSources()
 	CALL slot.add(dsHandle)
    
    LET html_opts = HTMLRenderOption.create()    
    CALL html_opts.setOutputFileName(filename)

	LET task = engine.createRunAndRenderTask(design)
	CALL task.setRenderOption(html_opts)	
    DISPLAY "HTML render task is running..."
	CALL task.run()

	CALL task.close()
	CALL engine.destroy()	        
	CALL Platform.shutdown()
    DISPLAY "Report ",filename," has been saved on app server"
END FUNCTION

This demo project is intended to show and display BIRT integration with Lycia.

You can learn how to use BIRT reporting tools from LyciaStudio and for your 4gl projects from Lycia online documentation:
* BIRT perspective - https://querix.com/go/lycia/index.htm#05_workbench/01_ls/02_interface/01_perspectives/birt.htm
* BIRT views - https://querix.com/go/lycia/index.htm#05_workbench/01_ls/02_interface/02_views/birt/00_birt_views.htm
* BIRT integration - https://querix.com/go/lycia/index.htm#09_auxiliary/birt/00_birt.htm
* Creating BIRT reports - https://querix.com/go/lycia/index.htm#09_auxiliary/birt/usage/00_birt_usage_cover.htm
* Examples - https://querix.com/go/lycia/index.htm#09_auxiliary/birt/birt_examples/00_birt_exs.htm


To be able to try and use this project, you have to follow these steps:
1. Register at https://querix.com/signup/
2. Download the latest Lycia version: https://querix.com/products/lycia/downloads/
3. Install Lycia to your computer: https://querix.com/go/lycia/index.htm#01_qpm/01_cover.htm
4. Activate the trial license (1 license per 1 developer): https://querix.com/go/lycia/index.htm#02_licensing/01_trial.htm (Please contact us if you need more compilation seats. We'd also appreciate it if you let us know the number of developers who will be engaged in this project.)
5. Import this project from the GIT repository via LyciaStudio: https://querix.com/go/lycia/index.htm#05_workbench/01_ls/04_how_to/10_repo/git/local/clone_repo.htm

Use the link below to download the source files. Here is the link to be cloned: https://gitlab.com/QuerixTraining/BIRT.git

You can read about working with GIT repositories in Lycia online documentation: https://querix.com/go/lycia/index.htm#05_workbench/01_ls/04_how_to/10_repo/git/00_intro.htm

After importing this demo project to your workspace, you have to build and run it from LyciaStudio:
https://querix.com/go/lycia/index.htm#05_workbench/01_ls/04_how_to/03_build/build.htm
https://querix.com/go/lycia/index.htm#05_workbench/01_ls/04_how_to/06_run/run.htm

Demo usage:
1. Create and populate database:
	- in program create_db replace database name in variable 'connect_db_name' from 'auth' (file: create_db.4gl, line: 25) to any existing on your current Informix db server.
	- compile and run 'create_db' program
	- 'Recreate database'
2. Configure Informix JDBC driver
	- install Informix JDBC driver
	- add path to 'ifxjdbc.jar' file in CLASSPATH variable in 'inet.env' config
3. Configure Data Source in report design
	- Open invoice.rptdesign file in Report Design perspective
	- In Data Explorer tab go to Data Sources -> Data Source -> Edit
	- In BIRT JDBC Data Source tab click Manage Drives button. Add 'ifxjdbc.jar' driver and Press 'Ok'
	- Edit Database URL  (jdbc:informix-sqli://rh-informix2.qx:10012/birt_demo):  
		a) replace 'rh-informix2.qx' with your Informix DB Server name
		b) replace Informix DB Server port (10012)
	- Set your Informix DB Server user and password.
	- Press 'Test connection' to check database configurations
	- Close Data Source Edit window and save 'invoice.rptdesign' file 
4. Compile and Run 'BIRT Report Integrated into 4gl code' program

Please clear your browsing data before launching the application.


######################################

This project contains demo applications for Querix products.
This software is free but can be used and modified only with Lycia.
Feel free to give your suggestions about enriching this site. 
You can send your ideas and 4gl code to support@querix.com